#include <node.h>
#include "cache.h"

using namespace v8;


void AssembleAll(Handle<Object> exports) {
  CacheWrap::Assemble(exports);
}

NODE_MODULE(hadron, AssembleAll)
