var assert = require('assert');
var hadron = require('./build/Release/hadron');
var fs = require('fs');

var cache = hadron.createLRUCache(100);
cache.set('ckey1', 'cvalue1');

assert.equal(cache.get('ckey1'), 'cvalue1');
assert.equal(cache.get('none'), undefined);
console.log('Test success.');
